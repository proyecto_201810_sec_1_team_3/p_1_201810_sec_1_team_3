package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String id, taxiId;
	private long time;
	private double miles,cost;
	private RangoFechaHora rangoHora;
 
	

	public Servicio() {
	}


	public Servicio(String id, String taxiId, long time, double miles, double cost, RangoFechaHora rangoHora) {
		this.id = id;
		this.taxiId = taxiId;
		this.time = time;
		this.miles = miles;
		this.cost = cost;
		this.setRangoHora(rangoHora);
	}


	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the taxiId
	 */
	public String getTaxiId() {
		return taxiId;
	}


	/**
	 * @param taxiId the taxiId to set
	 */
	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}


	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}


	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}


	/**
	 * @return the miles
	 */
	public double getMiles() {
		return miles;
	}


	/**
	 * @param miles the miles to set
	 */
	public void setMiles(double miles) {
		this.miles = miles;
	}


	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}


	/**
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}


	public RangoFechaHora getRangoHora() {
		return rangoHora;
	}


	public void setRangoHora(RangoFechaHora rangoHora) {
		this.rangoHora = rangoHora;
	}
}
