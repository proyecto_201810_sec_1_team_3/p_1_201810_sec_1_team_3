package model.vo;

import model.data_structures.DoublyLinkedList;
import model.data_structures.ILinkedList;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private ILinkedList<Taxi> taxisInscritos;
	
	public Compania()
	{
		taxisInscritos= new DoublyLinkedList<>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ILinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(Taxi taxiInscrito) {
		this.taxisInscritos.add(taxiInscrito);
	}
	

	@Override
	public int compareTo(Compania o) 
	{
		int valorComparacion = nombre.compareToIgnoreCase( o.nombre );
		
	    if(valorComparacion < 0)
	    {
	        valorComparacion = -1;
	    }
	    
	    else if(valorComparacion == 0)
	    {
	        valorComparacion = 0;
	    }
	    
	    else
	    {
	        valorComparacion = 1;
	    }
	    
	    return valorComparacion;
	}
}
