package model.vo;

public class ServicioResumen extends Servicio
{
private double miles;
private String horaInicial;
private String horaFinal;
private double duracion;
private double valor;
private boolean acumulado;
public double getMiles() {
	return miles;
}
public void setMiles(double miles) {
	this.miles = miles;
}
public String getHoraInicial() {
	return horaInicial;
}
public void setHoraInicial(String horaInicial) {
	this.horaInicial = horaInicial;
}
public String getHoraFinal() {
	return horaFinal;
}
public void setHoraFinal(String horaFinal) {
	this.horaFinal = horaFinal;
}
public double getDuracion() {
	return duracion;
}
public void setDuracion(double duracion) {
	this.duracion = duracion;
}
public double getValor() {
	return valor;
}
public void setValor(double valor) {
	this.valor = valor;
}
public boolean isAcumulado() {
	return acumulado;
}
public void setAcumulado(boolean acumulado) {
	this.acumulado = acumulado;
}


}
