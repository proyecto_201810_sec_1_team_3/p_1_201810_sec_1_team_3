package model.vo;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoFechaHora implements Comparable<RangoFechaHora>
{
	//ATRIBUTOS

	/**
	 * Modela la fecha inicial del rango
	 */
	private String fechaInicial; 

	/**
	 * Modela la fecha final del rango
	 */
	private String fechaFinal;

	/**
	 * modela la hora inicial del rango
	 */
	private String horaInicio; 
	/**
	 * modela la hora final del rango
	 */
	private String horaFinal;


	private int anioInicial; 
	private int anioFinal;
	private int mesInicial;
	private int mesFinal;
	private int diaInicial;
	private int diaFinal;
	private int hIn;
	private int hFin;


	private int minutoInicial;
	private int minutoFinal;
	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 * @param pHoraInicio, hora inicial del rango
	 * @param pHoraFinal, hora final del rango
	 */
	public RangoFechaHora(String pFechaInicial, String pFechaFinal, String pHoraInicio, String pHoraFinal)
	{
		this.fechaFinal = pFechaFinal;
		this.fechaInicial = pFechaInicial;
		this.horaFinal = pHoraFinal;
		this.horaInicio = pHoraInicio;

		String[] fecha= pFechaInicial.split("-");
		anioInicial= Integer.valueOf(fecha[0]);
		mesInicial= Integer.valueOf(fecha[1]);
		diaInicial= Integer.valueOf(fecha[2]);

		String[] fechaFinal= pFechaFinal.split("-");
		anioFinal= Integer.valueOf(fechaFinal[0]);
		mesFinal= Integer.valueOf(fechaFinal[1]);
		diaFinal= Integer.valueOf(fechaFinal[2]);

		String[] horasinicio= pHoraInicio.split(":");
		hIn= Integer.valueOf(horasinicio[0]);
		minutoInicial= Integer.valueOf(horasinicio[1]);

		String[] horasfin= pHoraFinal.split(":");
		hFin= Integer.valueOf(horasfin[0]);
		minutoFinal= Integer.valueOf(horasfin[1]);

	}
	public RangoFechaHora()
	{

	}
	//M�TODOS

	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() 
	{
		return fechaInicial;
	}

	public int getMinutoInicial() {
		return minutoInicial;
	}
	public void setMinutoInicial(String minutoInicial) {
		this.minutoInicial = Integer.valueOf(minutoInicial);
	}
	public int getMinutoFinal() {
		return minutoFinal;
	}
	public void setMinutoFinal(String minutoFinal) {
		this.minutoFinal =Integer.valueOf( minutoFinal);
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial)
	{
		this.fechaInicial = fechaInicial;
		String[] fecha= fechaInicial.split("-");
		anioInicial= Integer.valueOf(fecha[0]);
		mesInicial= Integer.valueOf(fecha[1]);
		diaInicial= Integer.valueOf(fecha[2]);
	}

	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() 
	{
		return fechaFinal;
	}

	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String pfechaFinal) 
	{
		this.fechaFinal = pfechaFinal;
		String[] fecha= fechaFinal.split("-");
		anioFinal= Integer.valueOf(fecha[0]);
		mesFinal= Integer.valueOf(fecha[1]);
		diaFinal= Integer.valueOf(fecha[2]);

	}

	/**
	 * @return the horaInicio
	 */
	public String getHoraInicio() 
	{
		return horaInicio;
	}

	/**
	 * @param horaInicio the horaInicio to set
	 */
	public void setHoraInicio(String horaInicio) 
	{
		this.horaInicio = horaInicio;
		String[] horasinicio= horaInicio.split(":");
		hIn= Integer.valueOf(horasinicio[0]);
		minutoInicial= Integer.valueOf(horasinicio[1]);
	}

	/**
	 * @return the horaFinal
	 */
	public String getHoraFinal() 
	{
		return horaFinal;
	}

	/**
	 * @param horaFinal the horaFinal to set
	 */
	public void setHoraFinal(String horaFinal) 
	{
		this.horaFinal = horaFinal;

		String[] horasfin= horaFinal.split(":");
		hFin= Integer.valueOf(horasfin[0]);
		minutoFinal= Integer.valueOf(horasfin[1]);
	}
	public boolean inRange(RangoFechaHora rango)
	{

		boolean ans=false;
		if(startsInRange(rango)&&endsInRange(rango))
			ans=true;
		return ans;
	}
	public boolean startsInRange(RangoFechaHora rango)
	{
		boolean ans=false;

		if(compareTo(rango)==0 || compareTo(rango)==1)
		{
			ans=true;
		}
		return ans;
	}

	public boolean endsInRange(RangoFechaHora rango)
	{
		boolean ans=false;
		if(rango.anioFinal==anioFinal)
		{
			if(rango.mesFinal==mesFinal)
			{
				if(rango.diaFinal==diaFinal)
				{
					if(rango.hFin==hFin)
					{
						if(rango.minutoFinal==minutoFinal)
						{
							ans=true;
						}
						else if(rango.minutoFinal>minutoFinal)
						{
							ans=true;
						}

					}
					else if(rango.hFin>hFin)
					{
						ans=true;
					}
				}
				else if(rango.diaFinal>diaFinal)
				{
					ans=true;
				}
			}
			else if(rango.mesFinal>mesFinal)
			{
				ans=true;
			}
		}
		else if(rango.anioFinal>anioFinal)
		{
			ans=true;
		}
		return ans;
	}

	@Override
	public int compareTo(RangoFechaHora o) {
		// TODO Auto-generated method stub
		int ans=-1;
		if(o.anioInicial==anioInicial)
		{
			if(o.mesInicial==mesInicial)
			{
				if(o.diaInicial==diaInicial)
				{
					if(o.hIn==hIn)
					{
						if(o.minutoInicial==minutoInicial)
						{
							ans=0;
						}
						else if(o.minutoInicial<minutoInicial)
						{
							ans=1;
						}

					}
					else if(o.hIn<hIn)
					{
						ans=1;
					}
				}
				else if(o.diaInicial<diaInicial)
				{
					ans=1;
				}
			}
			else if(o.mesInicial<mesInicial)
			{
				ans=1;
			}
		}
		else if(o.anioInicial<anioInicial)
		{
			ans=1;
		}
		return ans;
	}

}
