package model.vo;

import model.data_structures.DoublyLinkedList;
import model.data_structures.ILinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String company;
	private String id;
	private ILinkedList<Servicio> listaServicios;
	public Taxi()
	{
		listaServicios= new DoublyLinkedList<>();
	}
	/**
	 * @return id - taxi_id
	 */
	
	
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public double getRentabilidad()
	{ double plata=0;
	double distancia=0;
		for (Servicio servicio : listaServicios) 
		{
			plata+= servicio.getCost();
			distancia+=servicio.getMiles();
		}
		return plata/distancia;
	}
	/**
	 * @return the listaServicios
	 */
	public ILinkedList<Servicio> getListaServicios() {
		return listaServicios;
	}
	/**
	 * @param listaServicios the listaServicios to set
	 */
	public void setListaServicios(Servicio listaServicios) {
		this.listaServicios.add(listaServicios);
	}	
}