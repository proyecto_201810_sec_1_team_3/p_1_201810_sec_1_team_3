package model.data_structures;

/******************************************************************************
 *  Compilation:  java DoublyLinkedList.java
 *  Execution:    java DoublyLinkedList
 *  Dependencies: StdOut.java
 *
 *  A list implemented with a doubly linked list. The elements are stored
 *  (and iterated over) in the same order that they are inserted.
 *
 ******************************************************************************/

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<Item> implements Iterable<Item>, ILinkedList<Item> 
{
	private int n;        // number of elements on list
	private Node pre;     // sentinel before first item
	
	private Node post;    // sentinel after last item

	public DoublyLinkedList() {
		pre  = new Node();
		post = new Node();
		pre.next = post;
		post.prev = pre;
	}

	// linked list node helper data type
	public class Node {
		private Item item;
		private Node next;
		private Node prev;
		public Node getNext(){return next;}
		public Node getPrev(){return prev;}
		public Item getItem(){return item;}
		public void setNext(Node n){next=n;}
		public void setPrev(Node p){prev=p;}
		public void setItem(Item i){item=i;}
	}

	public boolean isEmpty()    { return n == 0; }
	public int size()           { return n;      }
	public Item get(int i)
	{
		Item ans= null;
		int contador=-1;
		ListIterator<Item> iter= iterator();
		
		while (iter.hasNext())
		{
			Item actual= (Item)iter.next();
			contador++;
			if(contador==i)
			{
				ans=actual;
				break;
			}
		}
		return ans;
	}

	// add the item to the list
	public void add(Item item) {
		Node last = post.prev;
		Node x = new Node();
		x.item = item;
		x.next = post;
		x.prev = last;
		post.prev = x;
		last.next = x;
		n++;
	}
	

	public DoublyLinkedListIterator iterator()  { return new DoublyLinkedListIterator(); }

	// assumes no calls to DoublyLinkedList.add() during iteration
	public class DoublyLinkedListIterator implements ListIterator<Item> {
		private Node current      = pre.next;  // the node that is returned by next()
		private Node lastAccessed = null;      // the last node to be returned by prev() or next()
		// reset to null upon intervening remove() or add()
		private int index = 0;

		public boolean hasNext()      { return index < n; }
		public boolean hasPrevious()  { return index > 0; }
		public int previousIndex()    { return index - 1; }
		public int nextIndex()        { return index;     }

		public Item next() {
			if (!hasNext()) throw new NoSuchElementException();
			lastAccessed = current;
			Item item = current.item;
			current = current.next; 
			index++;
			return item;
		}
		public Node nextNode() {
			if (!hasNext()) throw new NoSuchElementException();
			lastAccessed = current;
			Node item = current;
			current = current.next; 
			index++;
			return item;
		}

		public Item previous() {
			if (!hasPrevious()) throw new NoSuchElementException();
			current = current.prev;
			index--;
			lastAccessed = current;
			return current.item;
		}

		// replace the item of the element that was last accessed by next() or previous()
		// condition: no calls to remove() or add() after last call to next() or previous()
		public void set(Item item) {
			if (lastAccessed == null) throw new IllegalStateException();
			lastAccessed.item = item;
		}

		// remove the element that was last accessed by next() or previous()
		// condition: no calls to remove() or add() after last call to next() or previous()
		public void remove() { 
			if (lastAccessed == null) throw new IllegalStateException();
			Node x = lastAccessed.prev;
			Node y = lastAccessed.next;
			x.next = y;
			y.prev = x;
			n--;
			if (current == lastAccessed)
				current = y;
			else
				index--;
			lastAccessed = null;
		}

		// add element to list 
		public void add(Item item) {
			Node x = current.prev;
			Node y = new Node();
			Node z = current;
			y.item = item;
			x.next = y;
			y.next = z;
			z.prev = y;
			y.prev = x;
			n++;
			index++;
			lastAccessed = null;
		}
		

	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		for (Item item : this)
			s.append(item + " ");
		return s.toString();
	}

	public void swap(Node a, Node b) {
		// TODO Auto-generated method stub
		Item a1= a.getItem();
		a.setItem(b.getItem());
		b.setItem(a1);
		
	}
	public Node getNode(Item i)
	{
		Node ans= null;
		DoublyLinkedList<Item>.DoublyLinkedListIterator iter= iterator();
		while(iter.hasNext())
		{
			Node nActual=iter.nextNode();
			Item actual=iter.next();
			if(actual.equals(i))
			{
				ans=nActual;
			}
		}
		return ans;
	}

}
