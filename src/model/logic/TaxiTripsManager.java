package model.logic;

import java.io.BufferedReader; 

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.ListIterator;

import javax.sound.midi.Synthesizer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import api.ITaxiTripsManager;
import model.data_structures.*;
import model.data_structures.DoublyLinkedList.DoublyLinkedListIterator;
import model.data_structures.DoublyLinkedList.Node;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import sun.awt.util.IdentityLinkedList;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public static final String FECHA = "fecha";
	public static final String DISTANCIA = "distancia";
	public static final String ALFABETICA = "alfa";
	public static final String NUMERO = "numero";
	public static final String FECHA_SERVICIO = "fechaServicio";


	private ILinkedList<Compania> listaCompanias;
	private ILinkedList<Servicio> listaServicios;
	private ILinkedList<Taxi>     listaTaxis;
	private ILinkedList<ZonaServicios> listaZonasInicioServicios;
	private ILinkedList<ZonaServicios> listaZonasFinServicios;
	private ILinkedList<CompaniaTaxi> companiasTaxiRentable;

	@Override //1C
	//COMPLEJIDAD O(N) 
	public boolean cargarSistema(String direccionJson) 
	{	
		listaTaxis= new DoublyLinkedList<>();
		listaServicios= new DoublyLinkedList<>();
		listaCompanias= new DoublyLinkedList<>();
		listaZonasInicioServicios = new DoublyLinkedList<>();
		listaZonasFinServicios = new DoublyLinkedList<>();
		companiasTaxiRentable= new DoublyLinkedList<>();
		boolean ans=false;
		try {

			JsonReader reader = new JsonReader(new FileReader(direccionJson));
			reader.beginArray();
			reader.beginObject();

			Taxi tNuevo= new Taxi();
			Servicio sNuevo = new Servicio();
			RangoFechaHora rango= new RangoFechaHora();
			ZonaServicios zonaInicio= new ZonaServicios();
			ZonaServicios zonaFin= new ZonaServicios();
			String fechaParaZona="";

			while (reader.hasNext()) {

				String name = reader.nextName();

				if (name.equals("company")) {
					String com = reader.nextString();
					tNuevo.setCompany(com);

				} 
				else if (name.equals("taxi_id")) {
					String tId = reader.nextString();
					tNuevo.setId(tId);
					sNuevo.setTaxiId(tId);
				}
				else if (name.equals("trip_id")) {
					String tId = reader.nextString();
					sNuevo.setId(tId);
				}				
				else if (name.equals("fare") ) {
					double f= reader.nextDouble();
					sNuevo.setCost(f);
				} 
				else if (name.equals("trip_seconds") ) {
					long t= reader.nextLong();
					sNuevo.setTime(t);
				} 
				else if (name.equals("trip_miles") ) {
					double sm= reader.nextDouble();
					sNuevo.setMiles(sm);
				} 
				else if (name.equals("trip_start_timestamp")) {
					String  f= reader.nextString();
					fechaParaZona= f;
					String[] fecha= f.split("T");

					rango.setFechaInicial(fecha[0]);
					rango.setHoraInicio(fecha[1]);

				}
				else if (name.equals("trip_end_timestamp")) {
					String  f= reader.nextString();
					String[] fecha= f.split("T");

					rango.setFechaFinal(fecha[0]);

					rango.setHoraFinal(fecha[1]);
				}
				else if (name.equals("pickup_community_area")) {
					String  id= reader.nextString();
					zonaInicio.setIdZona(id);
				}

				else if (name.equals("dropoff_community_area")) {
					String  id= reader.nextString();
					zonaFin.setIdZona(id);
				}

				else if(name.equals("trip_total"))
				{

					//Para que no muera
					String leer= reader.nextString();

					//agregar taxis y servicios

					sNuevo.setRangoHora(rango);
					listaServicios.add(sNuevo);

					if(isTaxi(tNuevo.getId(), listaTaxis)==null)
					{
						tNuevo.setListaServicios(sNuevo);
						listaTaxis.add(tNuevo);
					}
					else
					{
						Taxi actual=isTaxi(tNuevo.getId(), listaTaxis);
						actual.setListaServicios(sNuevo);
					}



					//Crear companias o agregar taxi a las companias ya existentes y  agregar su taxi m�s rentable
					if(tNuevo.getCompany()!=null)
					{
						if(isCompany(tNuevo.getCompany(), listaCompanias)!=null)
						{
							Compania cActual=isCompany(tNuevo.getCompany(), listaCompanias);
							ILinkedList<Taxi> listaActual= cActual.getTaxisInscritos();

							CompaniaTaxi compaT= isCompanyTaxi(tNuevo.getCompany(), companiasTaxiRentable);
							Taxi taxi= compaT.getTaxi();
							if(taxi!= null)
							{
								double mayor= taxi.getRentabilidad();
								if(tNuevo.getRentabilidad()>mayor)
								{
									compaT.setTaxi(tNuevo);
								}
							}
							if(isTaxi(tNuevo.getId(), listaActual)==null)
							{
								listaActual.add(tNuevo);
							}
						}
						else
						{
							Compania cNueva= new Compania();
							cNueva.setNombre(tNuevo.getCompany());
							cNueva.setTaxisInscritos(tNuevo);
							listaCompanias.add(cNueva);

							//companiaTaxi

							CompaniaTaxi compaT= new CompaniaTaxi();
							compaT.setNomCompania(tNuevo.getCompany());
							compaT.setTaxi(tNuevo);
							companiasTaxiRentable.add(compaT);
						}
					}
					//crea la lista zona servicios
					if(zonaInicio.getIdZona()!=null)
					{
						if(isZona(zonaInicio.getIdZona(), listaZonasInicioServicios)!=null)
						{
							ZonaServicios zActual=isZona(zonaInicio.getIdZona(), listaZonasInicioServicios);
							if(isFechaServicios(fechaParaZona, zActual.getFechasServicios())==null)
							{
								FechaServicios agregar= new FechaServicios();
								agregar.setFecha(fechaParaZona);
								agregar.setServiciosAsociados(sNuevo);
								zActual.getFechasServicios().add(agregar);
							}
						}
						else
						{

							ILinkedList<FechaServicios> listaFechas= new DoublyLinkedList<>();
							FechaServicios agregar= new FechaServicios();
							agregar.setFecha(fechaParaZona);
							agregar.setServiciosAsociados(sNuevo);
							listaFechas.add(agregar);
							zonaInicio.setFechasServicios(listaFechas);
							listaZonasInicioServicios.add(zonaInicio);
						}
					}

					if(zonaFin.getIdZona()!=null)
					{
						if(isZona(zonaFin.getIdZona(), listaZonasFinServicios)!=null)
						{
							ZonaServicios zActual=isZona(zonaFin.getIdZona(), listaZonasFinServicios);
							if(isFechaServicios(fechaParaZona, zActual.getFechasServicios())==null)
							{
								FechaServicios agregar= new FechaServicios();
								agregar.setFecha(fechaParaZona);
								agregar.setServiciosAsociados(sNuevo);
								zActual.getFechasServicios().add(agregar);
							}
						}
						else
						{

							ILinkedList<FechaServicios> listaFechas= new DoublyLinkedList<>();
							FechaServicios agregar= new FechaServicios();
							agregar.setFecha(fechaParaZona);
							agregar.setServiciosAsociados(sNuevo);
							listaFechas.add(agregar);
							zonaFin.setFechasServicios(listaFechas);
							listaZonasFinServicios.add(zonaFin);
						}
					}

					//Finaliza un objeto y empieza otro
					tNuevo= new Taxi();
					sNuevo= new Servicio();
					rango= new RangoFechaHora();
					zonaInicio= new ZonaServicios();
					zonaFin = new ZonaServicios();
					fechaParaZona="";

					reader.endObject();
					if(reader.peek().equals(JsonToken.BEGIN_OBJECT))
					{
						reader.beginObject();
					}
					else
					{
						reader.endArray();

						ans=true;
						break;
					}

				}

				else {
					reader.skipValue();
				}
			}
			reader.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		} 

		//para comprobar que las cosas se cargan bien, coloca lo que quieras adentro
		//for (ZonaServicios zonaServicios : listaZonasFinServicios) {
		//System.out.println(zonaServicios.getIdZona());
		//}
		return ans;

	}
	//COMPLEJIDAD N
	public Taxi isTaxi(String id, ILinkedList<Taxi> listaTaxis2)
	{
		Taxi ans=null;
		ListIterator<Taxi> iter = listaTaxis2.iterator();

		while(iter.hasNext())
		{
			Taxi actual = iter.next();

			if(actual!=null)
			{
				if(actual.getId().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}

		return ans;
	}
	//COMPLEJIDAD N
	public Servicio isServicio(String id, DoublyLinkedList<Servicio> lista)
	{
		Servicio ans=null;
		for(Servicio actual: lista)
		{
			if(actual!=null)
			{
				if(actual.getId().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}
		return ans;
	}
	//COMPLEJIDAD N
	public Compania isCompany(String id, ILinkedList<Compania> listaCompanias2)
	{
		Compania ans=null;
		ListIterator<Compania> iter = listaCompanias2.iterator();

		while(iter.hasNext())
		{
			Compania actual = iter.next();

			if(actual!=null)
			{
				if(actual.getNombre().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}
		return ans;
	}
	//COMPLEJIDAD N
	public CompaniaTaxi isCompanyTaxi(String id, ILinkedList<CompaniaTaxi> listaCompanias2)
	{
		CompaniaTaxi ans=null;
		ListIterator<CompaniaTaxi> iter = listaCompanias2.iterator();

		while(iter.hasNext())
		{
			CompaniaTaxi actual = iter.next();

			if(actual!=null)
			{
				if(actual.getNomCompania().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}
		return ans;
	}
	
	public ZonaServicios isZona(String id, ILinkedList<ZonaServicios> listaCompanias2)
	{
		ZonaServicios ans=null;
		ListIterator<ZonaServicios> iter = listaCompanias2.iterator();

		while(iter.hasNext())
		{
			ZonaServicios actual = iter.next();

			if(actual!=null)
			{
				if(actual.getIdZona().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}
		return ans;
	}
	
	public FechaServicios isFechaServicios(String id, ILinkedList<FechaServicios> listaCompanias2)
	{
		FechaServicios ans=null;
		ListIterator<FechaServicios> iter = listaCompanias2.iterator();

		while(iter.hasNext())
		{
			FechaServicios actual = iter.next();

			if(actual!=null)
			{
				if(actual.getFecha().equals(id))
				{
					ans=actual;
					break;
				}
			}
		}
		return ans;
	}

	@Override //1A
	//COMPLEJIDAD O(NLOG(N))
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		Queue<Servicio> ans= new Queue<Servicio>();
		Servicio[] servicios= new Servicio[listaServicios.size()];
		ListIterator<Servicio> iter= listaServicios.iterator();
		int contador=0;
		//N
		while(iter.hasNext())
		{
			servicios[contador]= iter.next();
			contador++;

		}
		//NLOG(N)
		sort(servicios, 0, servicios.length-1, FECHA);
		//N
		for(Servicio actual:servicios)
		{
			if(actual.getRangoHora().inRange(rango))
			{
				ans.enqueue(actual);
			}
		}

		System.out.println("Forward Traversal using next pointer");

		return ans;
	}
	void mergeCompaniasPorServicios(Object arr[], int l, int m, int r, RangoFechaHora rango)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		Compania L[] = new Compania [n1];
		Compania R[] = new Compania [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (Compania) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (Compania) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			int l1=0;
			for (Taxi taxi : L[i].getTaxisInscritos()) 
			{
				l1+=(serviciosEnTaxiPorRango(rango, taxi.getListaServicios()).size());
			}
			int l2=0;

			for (Taxi taxi : R[j].getTaxisInscritos()) 
			{
				l2+=(serviciosEnTaxiPorRango(rango, taxi.getListaServicios()).size());
			}


			if (l1 >l2)
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}
	void mergeServicioPorLetra(Object arr[], int l, int m, int r)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		Compania L[] = new Compania [n1];
		Compania R[] = new Compania [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (Compania) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (Compania) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i].getNombre().compareTo( R[j].getNombre()) <0)
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}
	void mergeServicioPorFecha(Object arr[], int l, int m, int r)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		Servicio L[] = new Servicio [n1];
		Servicio R[] = new Servicio [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (Servicio) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (Servicio) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i].getRangoHora().compareTo( R[j].getRangoHora()) <0)
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}
	void mergeServicioPorDistancia(Object arr[], int l, int m, int r)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		Servicio L[] = new Servicio [n1];
		Servicio R[] = new Servicio [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (Servicio) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (Servicio) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i].getMiles() < R[j].getMiles())
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}

	void mergeZona(Object arr[], int l, int m, int r)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		ZonaServicios L[] = new ZonaServicios[n1];
		ZonaServicios R[] = new ZonaServicios [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (ZonaServicios) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (ZonaServicios) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (Integer.parseInt(L[i].getIdZona()) < Integer.parseInt( R[j].getIdZona()))
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}

	void mergeFechaServicios(Object arr[], int l, int m, int r)
	{
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		FechaServicios L[] = new FechaServicios[n1];
		FechaServicios R[] = new FechaServicios [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = (FechaServicios) arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = (FechaServicios) arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i].getFecha().compareTo( R[j].getFecha()) <0)
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}


	void sort(Object arr[], int l, int r, String comparar)
	{
		if (l < r)
		{
			// Find the middle point
			int m = (l+r)/2;

			// Sort first and second halves
			sort(arr, l, m, comparar);
			sort(arr , m+1, r, comparar);

			// Merge the sorted halves
			if(comparar.equals(FECHA))
			{
				mergeServicioPorFecha(arr, l, m, r);
			}
			else if(comparar.equals(DISTANCIA))
			{
				mergeServicioPorDistancia(arr, l, m, r);
			}
			else if(comparar.equals(ALFABETICA))
			{
				mergeServicioPorLetra(arr, l, m, r);
			}
			else if(comparar.equals(NUMERO))
			{
				mergeZona(arr, l, m, r);
			}
			else if(comparar.equals(FECHA_SERVICIO))
			{
				mergeFechaServicios(arr, l, m, r);
			}
		}
	}
	void sortCompanias(Object arr[], int l, int r,  RangoFechaHora rango)
	{
		if (l < r)
		{
			// Find the middle point
			int m = (l+r)/2;

			// Sort first and second halves
			sortCompanias(arr, l, m, rango);
			sortCompanias(arr , m+1, r, rango);

			// Merge the sorted halves
			mergeCompaniasPorServicios(arr, l, m, r, rango);
		}

	}
	@Override //2A
	//O(N)
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		Taxi ans=new Taxi();
		int gloMax=0;
		// TODO Auto-generated method stub
		//N
		Compania com=isCompany(company, listaCompanias);
		if(com!=null)
		{
			ILinkedList<Taxi> taxis= com.getTaxisInscritos();


			for (Taxi taxi : taxis) 
			{
				int locMax= serviciosEnTaxiPorRango(rango, taxi.getListaServicios()).size();
				if(locMax>gloMax)
				{
					ans=taxi;
					gloMax=locMax;
				}
			}

		}
		else
		{
			System.out.println("No existe esa compa�ia");
		}
		System.out.println("El mayor n�mero de servicios es " + gloMax);
		return ans;

	}
	
	private ILinkedList<Servicio> serviciosEnTaxiPorRango(RangoFechaHora rango, ILinkedList<Servicio> servicios)
	{
		ILinkedList<Servicio> locMax=new DoublyLinkedList<Servicio>();
		for (Servicio actual : servicios) 
		{
			if(actual.getRangoHora().inRange(rango))
			{
				locMax.add(actual);
			}
		}
		return locMax;
	}
	@Override //3A
	//O(N)
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		InfoTaxiRango ans= null;
		Taxi taxi= isTaxi(id, listaTaxis);
		if(taxi!=null)
		{
			ans= new InfoTaxiRango();
			String compani=taxi.getCompany();
			double plata=0;
			ILinkedList<Servicio> servicios= serviciosEnTaxiPorRango(rango, taxi.getListaServicios());
			double distanciaTotalRecorrida=0;
			double tiempo=0;
			for (Servicio servicio : servicios) 
			{
				plata+= servicio.getCost();

				distanciaTotalRecorrida+= servicio.getMiles();
				tiempo+=servicio.getTime();
			}
			ans.setRango(rango);
			ans.setPlataGanada(plata);
			ans.setIdTaxi(id);
			ans.setDistanciaTotalRecorrida(distanciaTotalRecorrida);
			ans.setCompany(compani);
			ans.setServiciosPrestadosEnRango(servicios);
			ans.setTiempoTotal(tiempo +"");

		}
		else
		{
			System.out.println("No existe in Taxi con ese ID");
		}

		return ans;
	}

	@Override //4A
	//O(nlog(n))
	public ILinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		ILinkedList<RangoDistancia> ans = new DoublyLinkedList<>();
		int tam=0;
		Servicio[] servicios= new Servicio[listaServicios.size()];

		int contador=0;
		//Obtengo el numero de nodos y Creo vector para ordenarlo
		//N
		for (Servicio servicio : listaServicios) 
		{
			servicios[contador]= servicio;
			contador++;
			if(servicio.getMiles()>tam)
			{
				tam=(int)servicio.getMiles();
			}
		}
		
		//nlog(n)
		sort(servicios, 0, servicios.length-1, DISTANCIA);

		
		RangoDistancia rangoActual= new RangoDistancia();
		ILinkedList<Servicio> serviciosRango= new DoublyLinkedList<>();
		rangoActual.setServiciosEnRango(serviciosRango);
		int i=0;
		int j =0;

		rangoActual.setLimineInferior(j);
		rangoActual.setLimiteSuperior(j+1);
		//N
		while(i<servicios.length&& j<=tam)
		{
			Servicio actual= servicios[i];
			if(actual.getMiles()>=j&& actual.getMiles()<(j+1))
			{
				serviciosRango.add(actual);
				i++;

			}
			else
			{

				ans.add(rangoActual);
				j++;
				serviciosRango= new DoublyLinkedList<>();
				rangoActual=new RangoDistancia();
				rangoActual.setLimineInferior(j);
				rangoActual.setLimiteSuperior(j+1);

			}
		}

		//Agrega el ultimo
		rangoActual.setServiciosEnRango(serviciosRango);
		ans.add(rangoActual);

		return ans;

	}

	@Override //1B
	public ILinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		ILinkedList<Compania> companiasTaxisInscritos = new DoublyLinkedList<>();

		Compania[] companias= new Compania[listaCompanias.size()];
		ListIterator<Compania> iter= listaCompanias.iterator();
		int contador=0;
		while(iter.hasNext())
		{
			companias[contador]= iter.next();
			contador++;

		}

		// Lista Ordenada Complejidad: O(nLog(n))
		sort(companias, 0, companias.length-1, ALFABETICA);

		for(int i = 0; i < companias.length; i++)
		{
			Compania compania = (Compania) companias[i];
			companiasTaxisInscritos.add(compania);
		}

		return companiasTaxisInscritos;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{

		ILinkedList<Compania> companiasTaxisInscritos = darCompaniasTaxisInscritos();

		//Busqueda Binaria Complejidad: O(log(n))
		int posCompania = buscarBinarioCompaniaPorNombre( companiasTaxisInscritos ,nomCompania);

		System.out.println("Company: " + companiasTaxisInscritos.get(posCompania).getNombre());

		// Pedir lista de taxis inscritos a una compania
		//Complejidad: O(1)
		ILinkedList<Taxi> taxisInscritos = companiasTaxisInscritos.get(posCompania).getTaxisInscritos();

		// Variable que guarda el taxi que mayor facturaciÃ³n ha tenido hasta el momento.
		Taxi taxiMayorFacturacion = null;

		// Variable que guarde la mayor facturaciÃ³n hasta el momento.
		double facturacionEnRango = 0;

		//Variable temporal para realizar la comparacion de facturaciones.
		double temp = 0;

		// Recorrer la lista de taxis 
		//Complejidad O(n)
		for( int i =  0; i < taxisInscritos.size(); i++)
		{
			Taxi taxi = (Taxi) taxisInscritos.get(i);
			System.out.println( "Taxi ID: " + taxi.getId());

			// Pedir lista de servicios
			ILinkedList<Servicio> servicios = taxi.getListaServicios();

			// Recorrer lista de servicios
			// Recorrer lista de servicios O(n)
			for( int j = 0; j < servicios.size(); j++)
			{
				Servicio servicio = (Servicio) servicios.get(j);
				System.out.println( "Service ID: " + servicio.getId() + " In Range: " + servicio.getRangoHora().inRange(rango) + " Service Cost: " + servicio.getCost());

				// Encontrar los servicios del taxi que estan dentro del rango y sumar el costo
				if(servicio.getRangoHora().inRange(rango) == true)
				{
					temp += servicio.getCost();
				}
			}
			System.out.println(  "Facturacion: " + temp );

			if(temp > facturacionEnRango)
			{
				facturacionEnRango = temp;
				temp = 0;
				taxiMayorFacturacion = taxi;
			}
			else
			{
				temp = 0;
			}

		}

		System.out.println( "Mayor Facturacion: " + facturacionEnRango);
		return taxiMayorFacturacion;
	}


	public int buscarBinarioCompaniaPorNombre( ILinkedList<Compania> companiasOrdenadas,String nombreCompania )
	{
		int posicion = -1;
		int inicio = 0;
		int fin = companiasOrdenadas.size( ) - 1;

		boolean encontrado = false;

		while( inicio <= fin && encontrado == false )
		{
			int medio = ( inicio + fin ) / 2;


			Compania mitad = ( Compania ) companiasOrdenadas.get( medio );

			if( mitad.getNombre().compareTo( nombreCompania ) == 0 )
			{
				encontrado = true;
			}
			else if( mitad.getNombre().compareTo( nombreCompania ) > 0 )
			{
				fin = medio - 1;

			}
			else
			{
				inicio = medio + 1;
			}

			posicion = medio;
		}


		System.out.println(posicion);
		return posicion;

	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		ServiciosValorPagado[] serviciosZonaValorTotal = new ServiciosValorPagado[3];

		ServiciosValorPagado servicioValorPagado1 = new ServiciosValorPagado(); 
		ServiciosValorPagado servicioValorPagado2 = new ServiciosValorPagado(); 
		ServiciosValorPagado servicioValorPagado3 = new ServiciosValorPagado(); 

		double valorAcumulado1 = 0;
		double valorAcumulado2 = 0;
		double valorAcumulado3 = 0;

		ILinkedList<ZonaServicios> zonasInicio = darZonasServicios(rango);
		ILinkedList<ZonaServicios> zonasFin = darZonasServicios2(rango);

		// Buscar zona en lista inicio
		int posicion = buscarBinarioZonaPorID( zonasInicio, idZona);
		ZonaServicios zonaBuscadaInicio = zonasInicio.get(posicion);

		ILinkedList<FechaServicios> fechasInicio = zonaBuscadaInicio.getFechasServicios();

		// Buscar zona en lista fin
		int posicion2 = buscarBinarioZonaPorID( zonasFin, idZona);
		ZonaServicios zonaBuscadaFin = zonasInicio.get(posicion2);

		ILinkedList<FechaServicios> fechasFin = zonaBuscadaFin.getFechasServicios();

		//El numero total de sercvicios que se recogieron en la zona y terminaron en la otra zona
		//Valor total pagado por los usuarios
		ILinkedList<Servicio> servicios1 = new DoublyLinkedList<>();

		//El numero total de servicios que se recogieron en otra zona y terminaron en la zona
		// Valor total pagado por los usuarios
		ILinkedList<Servicio> servicios2 = new DoublyLinkedList<>();

		//Total servicios iniciados en y terminados en la misma zona
		//Valor total pagado por los usuarios
		ILinkedList<Servicio> servicios3 = new DoublyLinkedList<>();

		for(int i = 0; i < fechasInicio.size(); i++)
		{
			FechaServicios fechaInicio = fechasInicio.get(i);
			ILinkedList<Servicio> serviciosInicioEnFecha = fechaInicio.getServiciosAsociados();

			for(int j = 0; j < fechasFin.size(); j++)
			{
				FechaServicios fechaFin = fechasFin.get(j);
				ILinkedList<Servicio> serviciosFinEnFecha = fechaFin.getServiciosAsociados();

				for(int k = 0; k < serviciosInicioEnFecha.size(); k++)
				{
					Servicio servicioInicio = serviciosInicioEnFecha.get(k);
					String serviceId = servicioInicio.getId();

					// Buscar el servicio en la lista de zonasFin
					int pos = buscarBinarioServicio( serviciosFinEnFecha, serviceId);
					Servicio servicioFin = serviciosFinEnFecha.get(pos);

					Servicio servicioFinal = serviciosFinEnFecha.get(k);
					String serviceId2 = servicioFinal.getId();

					// Buscar el servicio en la lista de zonasFin
					int pos2 = buscarBinarioServicio( serviciosInicioEnFecha, serviceId2);
					Servicio servicioInicio2 = serviciosFinEnFecha.get(pos);

					// Buscar el servicio en la lista de zonasInicio

					// Si esta,agregar a la lista 3
					// Si no, agregar a la lista 1
					if( servicioFin == null)
					{
						servicios1.add(servicioInicio);
						valorAcumulado1 += servicioInicio.getCost();
					}
					else if(servicioInicio.compareTo(servicioFin) == 0)
					{
						servicios3.add(servicioInicio);
						valorAcumulado3 += servicioInicio.getCost();
					}
					else if(servicioInicio2 == null)
					{
						servicios2.add(servicioFinal);
						valorAcumulado2 += servicioFinal.getCost();
					}
				}
			}
		}

		servicioValorPagado1.setServiciosAsociados(servicios1); 
		servicioValorPagado2.setServiciosAsociados(servicios2); 
		servicioValorPagado3.setServiciosAsociados(servicios3);
		servicioValorPagado1.setValorAcumulado(valorAcumulado1);
		servicioValorPagado2.setValorAcumulado(valorAcumulado2);
		servicioValorPagado3.setValorAcumulado(valorAcumulado3);

		serviciosZonaValorTotal[0] = servicioValorPagado1;
		serviciosZonaValorTotal[1] = servicioValorPagado2;
		serviciosZonaValorTotal[2] = servicioValorPagado3;

		return serviciosZonaValorTotal;
	}

	public int buscarBinarioZonaPorID( ILinkedList<ZonaServicios> zonasOrdenadas,String idZona )
	{
		int posicion = -1;
		int inicio = 0;
		int fin = zonasOrdenadas.size( ) - 1;

		boolean encontrado = false;

		while( inicio <= fin && encontrado == false )
		{
			int medio = ( inicio + fin ) / 2;


			ZonaServicios mitad = ( ZonaServicios ) zonasOrdenadas.get( medio );

			if( mitad.getIdZona().compareTo( idZona ) == 0 )
			{
				encontrado = true;
			}
			else if( mitad.getIdZona().compareTo( idZona ) > 0 )
			{
				fin = medio - 1;

			}
			else
			{
				inicio = medio + 1;
			}

			posicion = medio;
		}


		System.out.println(posicion);
		return posicion;

	}

	public int buscarBinarioServicio( ILinkedList<Servicio> serviciosOrdenados,String idServicio )
	{
		int posicion = -1;
		int inicio = 0;
		int fin = serviciosOrdenados.size( ) - 1;

		boolean encontrado = false;

		while( inicio <= fin && encontrado == false )
		{
			int medio = ( inicio + fin ) / 2;


			Servicio mitad = ( Servicio ) serviciosOrdenados.get( medio );

			if( mitad.getId().compareTo( idServicio ) == 0 )
			{
				encontrado = true;
			}
			else if( mitad.getId().compareTo( idServicio ) > 0 )
			{
				fin = medio - 1;

			}
			else
			{
				inicio = medio + 1;
			}

			posicion = medio;
		}


		System.out.println(posicion);
		return posicion;

	}

	@Override //4B
	public ILinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// Lista con todas las zonas de la ciudad (ordenadas por su identificador). total de servicios iniciados en dicha zona
		ILinkedList<ZonaServicios> zonasServicios = new DoublyLinkedList<>();

		ZonaServicios[] zonas= new ZonaServicios[listaZonasInicioServicios.size()];
		ListIterator<ZonaServicios> iter= listaZonasInicioServicios.iterator();
		int contador=0;

		while(iter.hasNext())
		{
			zonas[contador]= iter.next();
			contador++;
		}

		//Ordena numericamente la lista de zonas (Funciona)
		// Complejidad O(nlog(n))
		sort(zonas, 0, zonas.length-1, NUMERO);

		//O(n)
		for(int i = 0; i < zonas.length; i++)
		{
			ZonaServicios zona = (ZonaServicios) zonas[i];
			zonasServicios.add(zona);

			//La lista de zonas esta ordenada
			//System.out.println(zona.getIdZona());

			ILinkedList<FechaServicios> fechasServicios = zona.getFechasServicios();
			ILinkedList<FechaServicios> fechasServiciosEnRango = new DoublyLinkedList<>(); 
			for(int j = 0; j < fechasServicios.size(); j++)
			{ 
				// La lista esta ordenada cronologicamente
				FechaServicios fechaServicios = (FechaServicios) fechasServicios.get(j);

				String fecha = fechaServicios.getFecha() + ":0";
				//System.out.println(fechaServicios.getFecha() + ":0");

				String rangoTmpInicio = rango.getFechaInicial() + "T" + rango.getHoraInicio() + ":" + rango.getMinutoInicial();
				//System.out.println(rango.getFechaInicial() + "T" + rango.getHoraInicio() + ":" + rango.getMinutoInicial());

				String rangoTmpFin = rango.getFechaFinal() + "T" + rango.getHoraFinal() + ":" + rango.getMinutoFinal();

				if( fecha.compareTo(rangoTmpInicio) >= 0 )
				{
					if(fecha.compareTo(rangoTmpFin) <= 0)
					{
						fechasServiciosEnRango.add(fechaServicios);
						zona.setFechasServicios(fechasServiciosEnRango);
						//System.out.println("Entro al if");

						//System.out.println(zona.getIdZona());
					}

				}

			}


		}

		return zonasServicios;
	}

	public ILinkedList<ZonaServicios> darZonasServicios2(RangoFechaHora rango)
	{
		// Lista con todas las zonas de la ciudad (ordenadas por su identificador). total de servicios iniciados en dicha zona
		ILinkedList<ZonaServicios> zonasServicios = new DoublyLinkedList<>();

		ZonaServicios[] zonas= new ZonaServicios[listaZonasFinServicios.size()];
		ListIterator<ZonaServicios> iter= listaZonasFinServicios.iterator();
		int contador=0;

		while(iter.hasNext())
		{
			zonas[contador]= iter.next();
			contador++;
		}

		//Ordena numericamente la lista de zonas (Funciona)
		// Complejidad O(nlog(n))
		sort(zonas, 0, zonas.length-1, NUMERO);

		//O(n)
		for(int i = 0; i < zonas.length; i++)
		{
			ZonaServicios zona = (ZonaServicios) zonas[i];
			zonasServicios.add(zona);

			//La lista de zonas esta ordenada
			//System.out.println(zona.getIdZona());

			ILinkedList<FechaServicios> fechasServicios = zona.getFechasServicios();
			ILinkedList<FechaServicios> fechasServiciosEnRango = new DoublyLinkedList<>(); 
			for(int j = 0; j < fechasServicios.size(); j++)
			{ 
				// La lista esta ordenada cronologicamente
				FechaServicios fechaServicios = (FechaServicios) fechasServicios.get(j);

				String fecha = fechaServicios.getFecha() + ":0";
				//System.out.println(fechaServicios.getFecha() + ":0");

				String rangoTmpInicio = rango.getFechaInicial() + "T" + rango.getHoraInicio() + ":" + rango.getMinutoInicial();
				//System.out.println(rango.getFechaInicial() + "T" + rango.getHoraInicio() + ":" + rango.getMinutoInicial());

				String rangoTmpFin = rango.getFechaFinal() + "T" + rango.getHoraFinal() + ":" + rango.getMinutoFinal();

				if( fecha.compareTo(rangoTmpInicio) >= 0 )
				{
					if(fecha.compareTo(rangoTmpFin) <= 0)
					{
						fechasServiciosEnRango.add(fechaServicios);
						zona.setFechasServicios(fechasServiciosEnRango);
						//System.out.println("Entro al if");

						//System.out.println(zona.getIdZona());
					}

				}

			}


		}

		return zonasServicios;
	}

	@Override //2C
	//O(nlog(n))
	public ILinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		ILinkedList<CompaniaServicios> ans= new DoublyLinkedList<>();
		// TODO Auto-generated method stub
		Compania[] companias= new Compania[listaCompanias.size()];
		ListIterator<Compania> iter= listaCompanias.iterator();
		int contador=0;
		while(iter.hasNext())
		{
			companias[contador]= iter.next();
			contador++;

		}
		//nlog(n)
		sortCompanias(companias, 0, companias.length-1, rango);
		
		
		for (int i =0; i<n&&i<companias.length; i++) 
		{
			CompaniaServicios comp= new CompaniaServicios();
			comp.setNomCompania(companias[i].getNombre());
			ILinkedList<Servicio> serv= new DoublyLinkedList<>();
			for (Taxi tax : companias[i].getTaxisInscritos()) 
			{
				for (Servicio servicio : tax.getListaServicios()) 
				{
					serv.add(servicio);
				}
			}
			comp.setServicios(serv);

			ans.add(comp);
		}

		return ans;
	}

	@Override //3C
	//o(1)
	public ILinkedList<CompaniaTaxi> taxisMasRentables()
	{
		return companiasTaxiRentable;
	}

	@Override //4C
	//O(n^2)
	public IStack <ServicioResumen> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		IStack <ServicioResumen> ans = new Stack<>();

		// El limite dado
		double limite = 1;
		System.out.println( limite);



		//Taxi buscado
		Taxi taxi= isTaxi(taxiId, listaTaxis);



		if(taxi!=null)
		{
			
			
			int contador=0;
			RangoFechaHora rango= new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
			Servicio[] servicios= new Servicio[serviciosEnTaxiPorRango(rango, taxi.getListaServicios()).size()];
			ListIterator<Servicio> iter= serviciosEnTaxiPorRango(rango, taxi.getListaServicios()).iterator();
			while(iter.hasNext())
			{ 
				Servicio ser=iter.next();
					servicios[contador]= ser;
					contador++;			
			}

			//Ordena los servicios cronologicamente
			sort(servicios, 0, servicios.length-1, FECHA);
			double distanciaAcum=0;
			for (Servicio servicio : servicios) 
			{
				ServicioResumen res= new ServicioResumen();
				res.setTaxiId(servicio.getTaxiId());
				res.setId(servicio.getId());
				res.setDuracion(servicio.getTime());
				res.setHoraFinal(servicio.getRangoHora().getHoraFinal());
				res.setHoraInicial(servicio.getRangoHora().getHoraInicio());
				res.setMiles(servicio.getMiles());
				res.setValor(servicio.getCost());
				System.out.println(res.getMiles());
				ans.push(res);
				distanciaAcum+=res.getMiles();


				if((distanciaAcum>limite))

				{

					if(!ans.isEmpty()&&ans.size()>=2)
					{
						ServicioResumen resumen= new ServicioResumen();
						double duracion= 0;
						double dis=0;
						String horaI="";
						String horaF="";
						double valor=0;

						//primero siempre es el mas reciente porque la lista estaba ordenada
						ServicioResumen sacado= ans.pop();
						duracion+=sacado.getDuracion();
						valor+=sacado.getValor();
						horaF= sacado.getHoraFinal();
						dis+=sacado.getMiles();
						if(!ans.peek().isAcumulado())
						{
						ServicioResumen sacado2= ans.pop();
						
						while(ans.size()>=1&&!ans.peek().isAcumulado())
						{
							
							duracion+=sacado2.getDuracion();
							valor+=sacado2.getValor();
							dis+= sacado2.getMiles();
							sacado2=ans.pop();

						}
						//el ultimo que tiene la primera fecha

						
						duracion+=sacado2.getDuracion();
						valor+=sacado2.getValor();
						horaI= sacado2.getHoraInicial();
						dis+=sacado2.getMiles();
						}

						//Si el primer servicio supera el rango, agrega un servicio resumen con parametros en 0 
						resumen.setDuracion(duracion);
						resumen.setHoraFinal(horaF);
						resumen.setHoraInicial(horaI);
						resumen.setMiles(dis);
						resumen.setValor(valor);
						resumen.setAcumulado(true);
						ans.push(resumen);
					}

					distanciaAcum=0;
				}

			}

		}

		return ans;
	}


}
